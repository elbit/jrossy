<?php get_header(); ?>



<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('page-wrapper'); ?> style="padding: 2rem 5rem;">

	
		<h1 class="entry-title"><?php the_title(); ?></h1>
	
	
	
		<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
		<?php the_content(); ?>
		

</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>



<?php //get_sidebar(); ?>

<?php get_footer(); ?>