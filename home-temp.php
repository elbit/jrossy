<?php get_header();/* Template Name: home */ ?>

<nav class="bit-row justify v-center home-nav">
		<a href="projects" class="box btn t-center t-v-center hover-up ">Projects</a>
		<a href="eventos" class="box btn t-center t-v-center hover-up ">Events</a>
		<a href="bio" class="box btn  t-center t-v-center hover-up ">Bio</a>
	</nav>

	
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div class="home-extra bit-row">
		
		<div class="home-extra_head">
			
			<?php if ( has_post_thumbnail() ) { the_post_thumbnail('square'); } ?>
		</div>
		<div class="home-extra_body">
			<a target="_blank" class="btn btn-big t-center t-v-center" href="http://www.beguesjazzcamp.com">
				<?php the_title(); ?></a>
			
			<p><?php the_excerpt(); ?></p>
		</div>
	</div>
		
	
<?php endwhile; endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>