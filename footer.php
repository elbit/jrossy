	
</div><!-- end wrapper -->

<style>
	.site-footer img {
		width:15px;
		color:white;
	}

	svg {color:white}
</style>

<footer  class="bit-wrapper site-footer" role="contentinfo">
	Jorge Rossy - <?php echo date('Y'); ?> //
		<a href="https://es-es.facebook.com/jorgerossymusic/" target="_blank">
		<span><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.svg" alt="facebook"></span>
			Facebook
		<!-- </a> //<a href="https://twitter.com/jordirossy" target="_blank">
			Twitter
		</a>  -->//
		</a> 
		<a href="https://www.instagram.com/jorgerossymusic" target="_blank">
		<span><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.svg" alt="instagram"></span>
			Instagram
		</a> 
		</a> //<a href="https://open.spotify.com/artist/0bUTRhTTU18M19hpsAPSrE?si=W6sDdy3BRX2hoZtVteO9eA" target="_blank">
		<span><img src="<?php echo get_template_directory_uri(); ?>/img/spotify.svg" alt="spotify"></span>
			Spotify
		</a>
		<span class="span-paiste"><img style="width:50px;" src="<?php echo get_template_directory_uri(); ?>/img/paiste-logo.png" alt="Paiste" ><a href="https://www.paiste.com/en/musicians/jorge-rossy" target="_blank" style="padding:0 10px"> Jorge Rossy plays Paiste cymbals</a></span>
		
	</footer>

		
<?php wp_footer(); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.2.0/jquery.fitvids.min.js"></script>
<script>
 jQuery(document).ready(function(){
    // Target your .container, .wrapper, .post, etc.
   jQuery(".content").fitVids();
  });
</script>

</body>
</html>