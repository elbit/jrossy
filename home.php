<?php get_header(); ?>

<nav class="bit-row  are-same-heigth and-v-center home-nav">
	<a href="projects" class="btn-transparent home-nav_item bit-column-1-3 column t-center t-v-center">Projects</a>
	<a href="eventos" class="btn-transparent home-nav_item bit-column-1-3 column t-center t-v-center ">Tour dates <br>& concerts</a>
	<a href="about" class="btn-transparent home-nav_item bit-column-1-3 column t-center t-v-center ">About</a>
</nav>

<div class="home-news">
	<div class="bit-row are-same-heigth and-v-center home-nav-news">
			<a href="news" class="btn-transparent home-news_btn t-center t-v-center">News</a>
	</div>

	<div class="bit-row">

		<?php $query = new WP_Query( 'cat=2' ); ?>
		<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
		
			<div class="home-news_item card-h bit-column-1-3">
				
				<div class="home-news_head card-h_head">
					<?php if ( has_post_thumbnail() ) { the_post_thumbnail('square'); } ?>
				</div>
				
				<div class="home-news_body card-h_body">
					
					<a class="t-center t-v-center card-h_title" href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
				</div>
			</div>
			
			<?php endwhile; 
	wp_reset_postdata();
	else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>
		
	</div>
</div>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>