<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
	<meta charset="<?php bloginfo('charset');?>" />
	<meta name="viewport" content="width=device-width" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<?php wp_head();?>
</head>

<body <?php body_class();?>>

<div class="bit-wrapper is-main" >

	<header class="bit-row bit-p-t-1 site-header" role="banner">

		
			<h1 class="site-title">
				<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_html(get_bloginfo('name')); ?>" rel="home"><?php echo esc_html(get_bloginfo('name')); ?></a>
			</h1>
	

		<nav  class="menu" role="navigation">
				<?php wp_nav_menu(array('theme_location' => 'main-menu'));?>
		</nav>

	</header>

