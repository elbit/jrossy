<?php
add_action( 'after_setup_theme', 'rossywp_setup' );

function rossywp_setup()
{
load_theme_textdomain( 'rossywp', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );

global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;

register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'rossywp' ) )
);
// add menu classes to anchor <a>, working but ugly
function my_walker_nav_menu_start_el($item_output, $item, $depth, $args) {
    $classes     = implode(' ', $item->classes);
    $item_output = preg_replace('/<a /', '<a class="'.$classes.'"', $item_output, 1);
    return $item_output;
 }
add_filter('walker_nav_menu_start_el', 'my_walker_nav_menu_start_el', 10, 4);

}

if ( function_exists( 'add_theme_support' ) ) {

    // Add Thumbnail Theme Support.
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'large', 700, '', true ); // Large Thumbnail.
    add_image_size( 'medium', 250, '', true ); // Medium Thumbnail.
    add_image_size( 'small', 120, '', true ); // Small Thumbnail.
    add_image_size( 'custom-size', 700, 200, true ); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
    add_image_size( 'square', 500, 400, true ); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use.
    /*add_theme_support('custom-background', array(
    'default-color' => 'FFF',
    'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use.
    /*add_theme_support('custom-header', array(
    'default-image'          => get_template_directory_uri() . '/img/headers/default.jpg',
    'header-text'            => false,
    'default-text-color'     => '000',
    'width'                  => 1000,
    'height'                 => 198,
    'random-default'         => false,
    'wp-head-callback'       => $wphead_cb,
    'admin-head-callback'    => $adminhead_cb,
    'admin-preview-callback' => $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    // Enable HTML5 support.
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

    // Localisation Support.
    load_theme_textdomain( 'html5blank', get_template_directory() . '/languages' );
}







add_action( 'wp_enqueue_scripts', 'rossywp_load_scripts' );
function rossywp_load_scripts()
{
wp_enqueue_script( 'jquery' );
}

add_action( 'comment_form_before', 'rossywp_enqueue_comment_reply_script' );
function rossywp_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}

add_filter( 'the_title', 'rossywp_title' );
function rossywp_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}

add_filter( 'wp_title', 'rossywp_filter_wp_title' );
function rossywp_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}

add_action( 'widgets_init', 'rossywp_widgets_init' );

function rossywp_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'rossywp' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}

function rossywp_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php 
}

add_filter( 'get_comments_number', 'rossywp_comments_number' );
function rossywp_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}

// GOOGLE MAPS API KEY FOR ACF

function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyDaVUrLJHDjl9VFNUcHvttgwIB68kdeL5I';
	
	return $api;
	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


// post formats

add_theme_support( 'post-formats', array( 'aside', 'gallery' ,'link') );

//acf gallery

//Create extra fields called Altnative Text and Custom Classess
function my_extra_gallery_fields( $args, $attachment_id, $field ){
    $args['alt'] = array('type' => 'text', 'label' => 'Altnative Text', 'name' => 'alt', 'value' => get_field($field . '_alt', $attachment_id) ); // Creates Altnative Text field
    $args['class'] = array('type' => 'text', 'label' => 'Custom Classess', 'name' => 'class', 'value' => get_field($field . '_class', $attachment_id) ); // Creates Custom Classess field
    return $args;
}
add_filter( 'acf_photo_gallery_image_fields', 'my_extra_gallery_fields', 10, 3 );



//https://wordpress.stackexchange.com/questions/197001/how-to-show-postmeta-in-custom-columns-for-the-posts-screen
add_filter( 'manage_events_posts_columns', 'set_custom_edit_events_columns' );    
add_action( 'manage_events_posts_custom_column' , 'custom_events_column', 10, 2 );

function set_custom_edit_events_columns($columns) {    
    unset( $columns['author'] );
    unset( $columns['date'] );
    $columns['hora'] = 'Event Date';
    //$columns['is_unless'] = 'Is Unless';
    
    return $columns;    
}

function custom_events_column( $column, $post_id ) {   
    global $post;
    switch ( $column ) {
        case 'hora' :
            if(get_field( "hora", $post_id )) {
                $date = new DateTime(get_field('hora'));
                echo $date->format('d-M-y');
            } else {
                echo 0;
            }
        break;

        // case 'is_unless' :
        //     if(get_field( "is_unless", $post_id )) {
        //         echo get_field( "is_unless", $post_id );
        //     } else {
        //         echo 0;
        //     }
        // break;    
    }   
}

function my_column_register_sortable( $columns ) {
     $columns['hora'] = 'hora';
    //$columns['is_unless'] = 'is_unless';
    return $columns;
}

add_filter("manage_edit-events_sortable_columns", "my_column_register_sortable" );
