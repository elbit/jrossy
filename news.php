<?php
/* Template Name: news*/
?>
<?php get_header(); ?>

<div class="bit-row">


<?php $loop = new WP_Query( array(  'posts_per_page' => -1,'order'=> 'DESC') ); ?>
<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

    <div class="epks-wrapper card-h bit-column-1-2">
		
		<div class="card-h_head">
		<?php the_post_thumbnail('square');?>
		</div>

		<div class="card-h_body">
			
			<h3 class="card-h_title">
				<a href="<?php the_permalink();?>"><?php the_title();?></a>
			</h3> 
			
			<?php if( get_field('Subtitle') ): ?>
			<h5 class="subtitle">
				<i><?php the_field('Subtitle');?></i>
			</h5>
			<?php endif; ?>
		
		</div>	

	</div>

<?php endwhile; wp_reset_query(); ?>
</div>

	
<?php get_footer(); ?>