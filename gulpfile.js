// // npm install gulp-sass gulp-minify-css gulp-livereload gulp-autoprefixer --save-dev

// var gulp = require('gulp');
// //plugins
// //var sass = require('gulp-sass');
// var sass = require('gulp-sass')(require('sass'));

// var minifyCSS = require('gulp-minify-css');
// var autoprefixer = require('gulp-autoprefixer');
// var livereload = require('gulp-livereload');
// var sourcemaps = require('gulp-sourcemaps');
// var notify = require('gulp-notify');
// // var svgmin = require('gulp-svgmin');
// var svgo = require('gulp-svgo');
// //var browserify = require('gulp-browserify');


// function errorAlertJS(error) {
//     //Aquí configuramos el título y subtítulo del mensaje de error, también el sonido.
//     notify.onError({
//         title: "Gulp sass",
//         subtitle: "Algo esta mal pavo",
//         sound: "Basso"
//     })(error);
//     //También podemos pintar el error en el terminal
//     console.log(error.toString());
//     this.emit("end");
// };



// // TASKS

// // gulp.task('svgo', function() {
 
// //     gulp.src('../img/icones/dev/*.svg')
// //         .pipe(svgo())
// //         .pipe(gulp.dest('../img/icones'));
// // });

// gulp.task('sass', function(){
//   return gulp.src('scss/style.scss')
//     .pipe(sourcemaps.init())
//     .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError)) 
//     .on("error", errorAlertJS)
//     .pipe(autoprefixer('last 3 versions'))
// 	.pipe(minifyCSS())
//   .pipe(sourcemaps.write())
// 	.pipe(gulp.dest(''))

//   .pipe(notify("bien hecho Dani"))
//   .pipe(livereload());
// });

// gulp.task('build', function() {
//     return gulp.src('scss/style.scss')

//     .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
//         .on("error", errorAlertJS)
//         .pipe(autoprefixer('last 3 versions'))
//         .pipe(minifyCSS())

//     .pipe(gulp.dest(''))

//     .pipe(notify("bien hecho Dani"))

//     .pipe(livereload());
// });


// // Gulp watch 
// gulp.task('watch',['sass'],function(){
//   livereload.listen();
//   gulp.watch('scss/**/*.scss', ['sass']); 
//   // Other watchers
//   gulp.watch('*.php',livereload.reload); 
// })

// 'use strict'
// const { src, dest, watch } = require('gulp');
// const sass = require('gulp-sass')(require('sass'));
// function compileSass(done) {
//   src('scss/style.scss')
//   .pipe(sass().on('error', sass.logError))
//   .pipe(dest('app/css'));
//  done();
// }
// function watchSass() {
//    watch('scss/style.scss', compileSass);
// }
// exports.watchSass = watchSass

'use strict'
const { src, dest } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
function compileSass(done) {
  src('scss/style.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(dest('style.css'));
 done();
}
exports.compileSass = compileSass

// comando para usarlo gulp compileSass