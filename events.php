<?php /* Template Name: Events */ ?>
<?php get_header(); ?>

<?php 	

	$today = date('Ymd');
    $args  = array(
		'post_type' => 'events', 
		'posts_per_page' => -1,
		'order'=> 'ASC',
		'orderby' => 'meta_value',
       
		'meta_key' => 'hora',
        
        'meta_query'     => array(
            array(
				'key'     =>  'hora',
				'compare' => '>=',
                'value'   => $today,
			),
            
        ),

    );
	$loop = new WP_Query( $args);
?>



<?php if  ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); ?>

<div class="event">
<?php $date = new DateTime(get_field('hora'));?>
<?php $date2 = new DateTime(get_field('dia_2'));?>		
			
	<span class="day"><span class="typcn typcn-calendar-outline"></span><?php echo $date->format('d'); ?></span>
	
	<span class="month"><?php echo $date->format('M'); ?></span>
	<span class="year"> - <?php echo $date->format('Y'); ?></span>
	<span class="hour"> - <?php the_field('hora_time');?></span>

	<?php if( get_field('dia_2') ): ?>/
		<span class="day"><span class="typcn typcn-calendar-outline"></span><?php echo $date2->format('d'); ?></span>
		<span class="month"><?php echo $date2->format('M'); ?></span>
	<?php endif; ?>

	<?php if( get_field('hora_time_2') ): ?>
		<?php the_field('hora_time_2');?>
	<?php endif; ?>
		
	

	<h5 class="name"><?php the_title();?></h5>
		
	<a class="place" target="_blank" href="https://www.google.com/maps/place/<?php $location = get_field('place'); echo $location['lat'] . ',' . $location['lng']; ?>"><span class="typcn typcn-location-outline"></span><?php $location = get_field('place'); echo $location['address']; ?></a>
		
	<a class="btn event_more-info" target="_blank" href="<?php the_field('link');?>">+<span class="typcn typcn-info-large-outline"></span> </a>
			
			
</div>

<?php endwhile; else : ?>
<div class="event">
	<?php esc_html_e( 'Sorry, no tour dates & concerts.' ); ?> More info at <a href="https://es-es.facebook.com/jorgerossymusic/" target="_blank"> Facebook </a> or  <a href="news">news page.</a> 
</div>
<?php endif; ?>

<?php wp_reset_query();?>
<?php get_footer(); ?>
