<?php
/* Template Name: EpKs */
?>
<?php get_header(); ?>



<h1 style="color:white">CURRENT</h1>

<div class="bit-row">

<?php $loop = new WP_Query( array( 
	'post_type' => 'epk', 
	'posts_per_page' => -1,
	'order'=> 'ASC',
	'category_name' => 'current') ); ?>

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

	<a  href="<?php the_permalink();?>" class="bit-column-1-2 card-h ">
			
			<div class="card-h_head">
			
			<?php the_post_thumbnail('square');?>
			
			</div>

			<div class="card-h_body">
				
				<h3 class="card-h_title ">
					<?php the_title();?>
				</h3> 
				
				<?php if( get_field('Subtitle') ): ?>
				<h5 class="subtitle  t-v-center">
					<i><?php the_field('Subtitle');?></i>
				</h5>
				<?php endif; ?>
			
			</div>	

		</a>

<?php endwhile; wp_reset_query(); ?>

</div>

<h1 style="color:white">PAST</h1>

<div class="bit-row">
	
<?php $loop = new WP_Query( array( 
	'post_type' => 'epk', 
	'posts_per_page' => -1,
	'order'=> 'ASC',
	'category_name' => 'past') ); ?>

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

	<a  href="<?php the_permalink();?>" class="bit-column-1-2 card-h ">
			
			<div class="card-h_head">
			
			<?php the_post_thumbnail('square');?>
			
			</div>

			<div class="card-h_body">
				
				<h3 class="card-h_title ">
					<?php the_title();?>
				</h3> 
				
				<?php if( get_field('Subtitle') ): ?>
				<h5 class="subtitle  t-v-center">
					<i><?php the_field('Subtitle');?></i>
				</h5>
				<?php endif; ?>
			
			</div>	

		</a>

<?php endwhile; wp_reset_query(); ?>

</div>

	
<?php get_footer(); ?>

