<?php get_header(); ?>

<section role="main" class="bit-copy-wrapper content">
	
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
	<h1><?php the_title('');?></h1>
	<h3 class="subtitle"><?php the_field('Subtitle');?></h3>
	<?php if( get_field('descripcion') ): ?>
		<p><?php the_field('descripcion');?></p>
	<?php endif; ?>
	
	<?php if( get_field('imagenes') ): ?>
		<h3><span class="typcn typcn-image-outline"></span> Images <small>(click to large img)</small></h3>
		<div class="bit-row">
		<?php
		//Get the images ids from the post_metadata
		$images = acf_photo_gallery('imagenes', $post->ID);
		//Check if return array has anything in it
		if( count($images) ):
			//Cool, we got some data so now let's loop over it
			foreach($images as $image):
				$id = $image['id']; // The attachment id of the media
				$title = $image['title']; //The title
				$caption= $image['caption']; //The caption
				$full_image_url= $image['full_image_url']; //Full size image url
				$full_image_url_thumb = acf_photo_gallery_resize_image($full_image_url, 262, 160); //Resized size to 262px width by 160px height image url
				$thumbnail_image_url= $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
				$url= $image['url']; //Goto any link when clicked
				$target= $image['target']; //Open normal or new tab
				$alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
				$class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
		?>

		
		
			<a class="bit-column-1-3" href="<?php echo $full_image_url; ?>" target="blank">
				<img src="<?php echo $full_image_url_thumb; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>">
			</a>
	
		
			</div>
	<?php endforeach; endif; ?>	
	<?php endif; ?>
	
	
	<?php if( get_field('videos') ): ?>
		<h3><span class="typcn typcn-social-youtube"> </span> Videos</h3>
		<p> <?php the_field('videos'); ?></p>
	<?php endif; ?>

	
	<?php if( get_field('rider') ): ?>
		<h3><span class="typcn typcn-document"></span> Docs</h3>
		<ul>
			<li>
				<a href="<?php the_field('rider');?>"><i class="typcn typcn-big typcn-document-text"></i>Rider</a>
			</li>
		</ul>
	<?php endif; ?>

	<?php if( get_field('press') ): ?>
		<h3><span class="typcn typcn-news"></span> Press</h3>
	<p> <?php the_field('press'); ?></p>
	<?php endif; ?>

	<?php if( get_field('contact') ): ?>
		<h3><span class="typcn typcn-mail"></span> Contact</h3>
		<p> <?php the_field('contact'); ?></p>
	<?php endif; ?>




<?php endwhile; endif; ?>
	
</section>



<section class="bit-section ">

	<h2 class="bit-text-negative t-center bit-m-t-2">Other Projects</h2>

	<div class="bit-row">

				<?php $loop = new WP_Query( array( 'post_type' => 'epk', 'posts_per_page' => 2,'orderby'   => 'rand','order'=> 'ASC') ); ?>
<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

	<a  href="<?php the_permalink();?>" class="bit-column-1-2 card-h ">
			
			<div class="card-h_head">
			
			<?php the_post_thumbnail('square');?>
			
			</div>

			<div class="card-h_body">
				
				<h3 class="card-h_title ">
					<?php the_title();?>
				</h3> 
				
				<?php if( get_field('Subtitle') ): ?>
				<h5 class="subtitle  t-v-center">
					<i><?php the_field('Subtitle');?></i>
				</h5>
				<?php endif; ?>
			
			</div>	

		</a>

<?php endwhile; wp_reset_query(); ?>
	</div>
</section>


<?php //get_sidebar(); ?>
<?php get_footer(); ?>


